# About
Wilcot Logging — a simple in usage PHP library for logging.

# Usage
```php
use Wilcot\Logging\Logger;
use Wilcot\Logging\Handlers\FileHandler;

// Create new logger instance
$logger = new Logger();

// Create new handler instance
$handler = new FileHandler('test.log');

// Attach handler to the logger
$logger->addHandler($handler);

// Use created logger
$logger->debug('Message 1');
$logger->error('Message 2');
$logger->log(Logger::INFO, 'Message 3');
```
