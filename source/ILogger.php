<?php namespace Wilcot\Logging;
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 * @package Wilcot\Logging
 */

/**
 * Interface ILogger
 *
 * @since 0.1.0
 */
interface ILogger
{
	/**
	 * Add handler
	 *
	 * @param IHandler $handler
	 * @return $this
	 */
	public function addHandler(IHandler $handler);

	/**
	 * Get list of handlers
	 *
	 * @return array
	 */
	public function getHandlers();

	/**
	 * Get minimal supported level
	 *
	 * @return int
	 */
	public function getLevel();

	/**
	 * Set minimal supported level
	 *
	 * @param int $level
	 * @return $this
	 */
	public function setLevel($level);

	/**
	 * Get string representation of level
	 *
	 * @param int $level
	 * @return string
	 */
	public function getLevelName($level);

	/**
	 * Set string representation of level
	 *
	 * @param int $level
	 * @param string $name
	 * @return $this
	 */
	public function setLevelName($level, $name);

	/**
	 * Log message
	 *
	 * @param int $level
	 * @param string $message
	 * @return $this
	 */
	public function log($level, $message);
};
