<?php namespace Wilcot\Logging;
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 * @package Wilcot\Logging
 */

/**
 * Interface IRecord
 *
 * @since 0.1.0
 */
interface IRecord
{
	/**
	 * Get logger that produced record
	 *
	 * @return ILogger
	 */
	public function getLogger();

	/**
	 * Get record level
	 *
	 * @return int
	 */
	public function getLevel();

	/**
	 * Get string representation of level
	 *
	 * @return string
	 */
	public function getLevelName();

	/**
	 * Get record message
	 *
	 * @return string
	 */
	public function getMessage();
};
