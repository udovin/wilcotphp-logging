<?php namespace Wilcot\Logging;
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 * @package Wilcot\Logging
 */

/**
 * Class Logger
 *
 * @since 0.1.0
 */
class Logger extends AbstractLogger
{
	const DEBUG = 10;
	const INFO = 20;
	const WARNING = 30;
	const ERROR = 40;
	const CRITICAL = 50;

	/**
	 * A constructor
	 *
	 * @param int $level
	 */
	public function __construct($level = 0)
	{
		parent::__construct($level);

		$this->_setupLevelNames();
	}

	/**
	 * Log message
	 *
	 * @param int $level
	 * @param string $message
	 * @return $this
	 */
	public function log($level, $message)
	{
		$record = new Record($this, $level, $message);

		return $this->handle($record);
	}

	/**
	 * Log debug message
	 *
	 * @param string $message
	 * @return $this
	 */
	public function debug($message)
	{
		return $this->log(self::DEBUG, $message);
	}

	/**
	 * Log info message
	 *
	 * @param string $message
	 * @return $this
	 */
	public function info($message)
	{
		return $this->log(self::INFO, $message);
	}

	/**
	 * Log warning message
	 *
	 * @param string $message
	 * @return $this
	 */
	public function warning($message)
	{
		return $this->log(self::WARNING, $message);
	}

	/**
	 * Log error message
	 *
	 * @param string $message
	 * @return $this
	 */
	public function error($message)
	{
		return $this->log(self::ERROR, $message);
	}

	/**
	 * Log critical message
	 *
	 * @param string $message
	 * @return $this
	 */
	public function critical($message)
	{
		return $this->log(self::CRITICAL, $message);
	}

	/**
	 * Setup names for all standard levels
	 */
	private function _setupLevelNames()
	{
		$this
			->setLevelName(self::DEBUG, 'Debug')
			->setLevelName(self::INFO, 'Info')
			->setLevelName(self::WARNING, 'Warning')
			->setLevelName(self::ERROR, 'Error')
			->setLevelName(self::CRITICAL, 'Critical');
	}
};
