<?php namespace Wilcot\Logging;
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 * @package Wilcot\Logging
 */

/**
 * Class AbstractHandler
 *
 * @since 0.1.0
 */
abstract class AbstractHandler implements IHandler
{
	/**
	 * @var IFormatter $_formatter
	 */
	private $_formatter;

	/**
	 * @var int $_level
	 */
	private $_level;

	/**
	 * A constructor
	 *
	 * @param IFormatter $formatter
	 * @param int $level
	 */
	public function __construct(
		IFormatter $formatter = null, $level = 0)
	{
		if (is_null($formatter))
		{
			$formatter = new Formatter();
		}

		$this->setFormatter($formatter);
		$this->setLevel($level);
	}

	/**
	 * Get formatter for records
	 *
	 * @return IFormatter
	 */
	public function getFormatter()
	{
		return $this->_formatter;
	}

	/**
	 * Set formatter for records
	 *
	 * @param IFormatter $formatter
	 * @return $this
	 */
	public function setFormatter(IFormatter $formatter)
	{
		$this->_formatter = $formatter;

		return $this;
	}

	/**
	 * Get minimal supported level
	 *
	 * @return int
	 */
	public function getLevel()
	{
		return $this->_level;
	}

	/**
	 * Set minimal supported level
	 *
	 * @param int $level
	 * @return $this
	 */
	public function setLevel($level)
	{
		$this->_level = $level;

		return $this;
	}

	/**
	 * Handle logger record
	 *
	 * @param IRecord $record
	 * @return $this
	 */
	public function handle(IRecord $record)
	{
		if ($this->getLevel() <= $record->getLevel())
		{
			$this->emit($record);
		}

		return $this;
	}

	/**
	 * Transform logger record into string
	 *
	 * @param IRecord $record
	 * @return string
	 */
	protected function format(IRecord $record)
	{
		return $this->getFormatter()->format($record);
	}

	/**
	 * Emit logger record to the specified place
	 *
	 * @param IRecord $record
	 * @return $this
	 */
	abstract protected function emit(IRecord $record);
};
