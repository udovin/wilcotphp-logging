<?php namespace Wilcot\Logging;
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 * @package Wilcot\Logging
 */

/**
 * Interface IHandler
 *
 * @since 0.1.0
 */
interface IHandler
{
	/**
	 * Get formatter for records
	 *
	 * @return IFormatter
	 */
	public function getFormatter();

	/**
	 * Set formatter for records
	 *
	 * @param IFormatter $formatter
	 * @return $this
	 */
	public function setFormatter(IFormatter $formatter);

	/**
	 * Get minimal supported level
	 *
	 * @return int
	 */
	public function getLevel();

	/**
	 * Set minimal supported level
	 *
	 * @param int $level
	 * @return $this
	 */
	public function setLevel($level);

	/**
	 * Handle logger record
	 *
	 * @param IRecord $record
	 * @return $this
	 */
	public function handle(IRecord $record);
};
