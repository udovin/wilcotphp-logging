<?php namespace Wilcot\Logging;
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 * @package Wilcot\Logging
 */

/**
 * Interface IFormatter
 *
 * @since 0.1.0
 */
interface IFormatter
{
	/**
	 * Transform logger record into string
	 *
	 * @param IRecord $record
	 * @return string
	 */
	public function format(IRecord $record);
};
