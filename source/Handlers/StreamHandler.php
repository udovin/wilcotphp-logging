<?php namespace Wilcot\Logging\Handlers;
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 * @package Wilcot\Logging\Handlers
 */

use Wilcot\Logging\AbstractHandler;
use Wilcot\Logging\IFormatter;
use Wilcot\Logging\IRecord;

/**
 * Class StreamHandler
 *
 * @since 0.1.0
 */
class StreamHandler extends AbstractHandler
{
	/**
	 * @var resource $_stream
	 */
	private $_stream;

	/**
	 * A constructor
	 *
	 * @param resource $stream
	 * @param IFormatter $formatter
	 * @param int $level
	 */
	public function __construct(
		$stream = null, IFormatter $formatter = null, $level = 0)
	{
		parent::__construct($formatter, $level);

		if (is_null($stream))
		{
			$stream = STDERR;
		}

		$this->setStream($stream);
	}

	/**
	 * Get native stream handle
	 *
	 * @return resource
	 */
	public function getStream()
	{
		return $this->_stream;
	}

	/**
	 * Set native stream handle
	 *
	 * @param resource $stream
	 * @return $this
	 */
	public function setStream($stream)
	{
		$this->_stream = $stream;

		return $this;
	}

	/**
	 * Transform logger record into string
	 *
	 * @param IRecord $record
	 * @return $this
	 */
	protected function emit(IRecord $record)
	{
		$message = $this->format($record) . PHP_EOL;

		// Acquire an exclusive lock
		if (flock($this->getStream(), LOCK_EX))
		{
			fwrite($this->getStream(), $message);
			fflush($this->getStream());

			// Release the lock
			flock($this->getStream(), LOCK_UN);
		}

		return $this;
	}
};
