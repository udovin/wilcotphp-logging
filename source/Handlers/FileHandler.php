<?php namespace Wilcot\Logging\Handlers;
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 * @package Wilcot\Logging\Handlers
 */

use RuntimeException;
use Wilcot\Logging\IFormatter;

/**
 * Class FileHandler
 *
 * @since 0.1.0
 */
class FileHandler extends StreamHandler
{
	/**
	 * A constructor
	 *
	 * @param string $file
	 * @param IFormatter $formatter
	 * @param int $level
	 * @throws RuntimeException
	 */
	public function __construct(
		$file, IFormatter $formatter = null, $level = 0)
	{
		$directory = dirname($file);

		if (!is_dir($directory))
		{
			@mkdir($directory, 0777, true);
		}

		$stream = @fopen($file, 'a');

		if ($stream === false)
		{
			throw new RuntimeException(
				sprintf('Unable to open file "%s".', $file)
			);
		}

		parent::__construct($stream, $formatter, $level);
	}

	/**
	 * A destructor
	 */
	public function __destruct()
	{
		fclose($this->getStream());
	}
};
