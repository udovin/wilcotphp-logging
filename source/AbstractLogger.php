<?php namespace Wilcot\Logging;
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 * @package Wilcot\Logging
 */

/**
 * Class AbstractLogger
 *
 * @since 0.1.0
 */
abstract class AbstractLogger implements ILogger
{
	/**
	 * @var array $_handlers
	 */
	private $_handlers;

	/**
	 * @var int $_level
	 */
	private $_level;

	/**
	 * @var array $_levelNames
	 */
	private $_levelNames;

	/**
	 * A constructor
	 *
	 * @param int $level
	 */
	public function __construct($level = 0)
	{
		$this->_handlers = array();
		$this->setLevel($level);
	}

	/**
	 * Add handler
	 *
	 * @param IHandler $handler
	 * @return $this
	 */
	public function addHandler(IHandler $handler)
	{
		array_push($this->_handlers, $handler);

		return $this;
	}

	/**
	 * Get list of handlers
	 *
	 * @return array
	 */
	public function getHandlers()
	{
		return $this->_handlers;
	}

	/**
	 * Get minimal supported level
	 *
	 * @return int
	 */
	public function getLevel()
	{
		return $this->_level;
	}

	/**
	 * Set minimal supported level
	 *
	 * @param int $level
	 * @return $this
	 */
	public function setLevel($level)
	{
		$this->_level = $level;

		return $this;
	}

	/**
	 * Get string representation of level
	 *
	 * @param int $level
	 * @return string
	 */
	public function getLevelName($level)
	{
		return $this->_levelNames[$level];
	}

	/**
	 * Set string representation of level
	 *
	 * @param int $level
	 * @param string $name
	 * @return $this
	 */
	public function setLevelName($level, $name)
	{
		$this->_levelNames[$level] = $name;

		return $this;
	}

	/**
	 * Handle record
	 *
	 * @param IRecord $record
	 * @return $this
	 */
	protected function handle(IRecord $record)
	{
		if ($this->getLevel() <= $record->getLevel())
		{
			foreach ($this->getHandlers() as $handler)
			{
				$handler->handle($record);
			}
		}

		return $this;
	}
};
