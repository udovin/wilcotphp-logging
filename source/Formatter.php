<?php namespace Wilcot\Logging;
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 * @package Wilcot\Logging
 */

/**
 * Class Formatter
 *
 * @since 0.1.0
 */
class Formatter implements IFormatter
{
	/**
	 * @var string $_format
	 */
	private $_format;

	/**
	 * @var string $_dateFormat
	 */
	private $_dateFormat;

	/**
	 * A constructor
	 *
	 * @param string $format
	 * @param string $dateFormat
	 */
	public function __construct($format = null, $dateFormat = null)
	{
		if (is_null($format))
		{
			$format = '{time} [{levelName}]: {message}';
		}

		if (is_null($dateFormat))
		{
			$dateFormat = 'd.m.Y H:i:s';
		}

		$this->_format = $format;
		$this->_dateFormat = $dateFormat;
	}

	/**
	 * Transform logger record into string
	 *
	 * @param IRecord $record
	 * @return string
	 */
	public function format(IRecord $record)
	{
		$array = array(
			'{time}' => date($this->_dateFormat),
			'{level}' => $record->getLevel(),
			'{levelName}' => $record->getLevelName(),
			'{message}' => $record->getMessage()
		);

		return strtr($this->_format, $array);
	}
};
