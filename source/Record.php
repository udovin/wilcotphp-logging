<?php namespace Wilcot\Logging;
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 * @package Wilcot\Logging
 */

/**
 * Class Record
 *
 * @since 0.1.0
 */
class Record implements IRecord
{
	/**
	 * @var ILogger $_logger
	 */
	private $_logger;

	/**
	 * @var int $_level
	 */
	private $_level;

	/**
	 * @var string $_message
	 */
	private $_message;

	/**
	 * A constructor
	 *
	 * @param ILogger $logger
	 * @param int $level
	 * @param string $message
	 */
	public function __construct(ILogger $logger, $level, $message)
	{
		$this->_logger = $logger;
		$this->_level = $level;
		$this->_message = $message;
	}

	/**
	 * Get logger that produced record
	 *
	 * @return ILogger
	 */
	public function getLogger()
	{
		return $this->_logger;
	}

	/**
	 * Get record level
	 *
	 * @return int
	 */
	public function getLevel()
	{
		return $this->_level;
	}

	/**
	 * Get string representation of level
	 *
	 * @return string
	 */
	public function getLevelName()
	{
		return $this->_logger->getLevelName($this->_level);
	}

	/**
	 * Get record message
	 *
	 * @return string
	 */
	public function getMessage()
	{
		return $this->_message;
	}
};
