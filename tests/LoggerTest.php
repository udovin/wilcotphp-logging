<?php
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 */

use PHPUnit\Framework\TestCase;
use Wilcot\Logging\Logger;

/**
 * Class LoggerTest
 *
 * @since 0.1.1
 */
class LoggerTest extends TestCase
{
	/**
	 * @var Logger $logger
	 */
	protected $logger;

	public function setUp()
	{
		$this->logger = new Logger();
	}

	public function tearDown()
	{
		unset($this->logger);
	}

	public function testBuiltInLevels()
	{
		$this->assertEquals(
			'Debug',
			$this->logger->getLevelName(Logger::DEBUG)
		);

		$this->assertEquals(
			'Info',
			$this->logger->getLevelName(Logger::INFO)
		);

		$this->assertEquals(
			'Warning',
			$this->logger->getLevelName(Logger::WARNING)
		);

		$this->assertEquals(
			'Error',
			$this->logger->getLevelName(Logger::ERROR)
		);

		$this->assertEquals(
			'Critical',
			$this->logger->getLevelName(Logger::CRITICAL)
		);
	}

	public function testCustomLevels()
	{
		$this->assertInstanceOf(
			get_class($this->logger),
			$this->logger->setLevelName(101, 'Level1')
		);

		$this->assertInstanceOf(
			get_class($this->logger),
			$this->logger->setLevelName(102, 'Level2')
		);

		$this->assertEquals('Level1', $this->logger->getLevelName(101));

		$this->assertEquals('Level2', $this->logger->getLevelName(102));
	}

	public function testBuiltInLogs()
	{
		$this->assertInstanceOf(
			get_class($this->logger),
			$this->logger->log(Logger::INFO, 'Message')
		);

		$this->assertInstanceOf(
			get_class($this->logger),
			$this->logger->debug('Message')
		);

		$this->assertInstanceOf(
			get_class($this->logger),
			$this->logger->info('Message')
		);

		$this->assertInstanceOf(
			get_class($this->logger),
			$this->logger->warning('Message')
		);

		$this->assertInstanceOf(
			get_class($this->logger),
			$this->logger->error('Message')
		);

		$this->assertInstanceOf(
			get_class($this->logger),
			$this->logger->critical('Message')
		);
	}
};
