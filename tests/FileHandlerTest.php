<?php
/**
 * @author Ivan Udovin
 * @license http://www.spdx.org/licenses/MIT
 */

use PHPUnit\Framework\TestCase;
use Wilcot\Logging\Logger;
use Wilcot\Logging\Record;
use Wilcot\Logging\Handlers\FileHandler;

/**
 * Class FileHandlerTest
 *
 * @since 0.1.1
 */
class FileHandlerTest extends TestCase
{
	/**
	 * @var string $file
	 */
	protected $file;

	/**
	 * @var FileHandler $handler
	 */
	protected $handler;

	public function setUp()
	{
		$this->file = tempnam(sys_get_temp_dir(), 'php');

		$this->handler = new FileHandler($this->file);
	}

	public function tearDown()
	{
		unset($this->handler);

		@unlink($this->file);
		unset($this->file);
	}

	public function testDebugLevel()
	{
		$record = new Record(new Logger(), Logger::DEBUG, 'Message');
		$string = $this->handler->getFormatter()->format($record);

		$this->handler->handle($record);
		$this->assertStringEqualsFile($this->file, $string . PHP_EOL);

		$this->handler->setLevel(Logger::DEBUG + 1);

		$this->handler->handle($record);
		$this->assertStringEqualsFile($this->file, $string . PHP_EOL);
	}

	public function testInfoLevel()
	{
		$record = new Record(new Logger(), Logger::INFO, 'Message');
		$string = $this->handler->getFormatter()->format($record);

		$this->handler->handle($record);
		$this->assertStringEqualsFile($this->file, $string . PHP_EOL);

		$this->handler->setLevel(Logger::INFO + 1);

		$this->handler->handle($record);
		$this->assertStringEqualsFile($this->file, $string . PHP_EOL);
	}

	public function testWarningLevel()
	{
		$record = new Record(new Logger(), Logger::WARNING, 'Message');
		$string = $this->handler->getFormatter()->format($record);

		$this->handler->handle($record);
		$this->assertStringEqualsFile($this->file, $string . PHP_EOL);

		$this->handler->setLevel(Logger::WARNING + 1);

		$this->handler->handle($record);
		$this->assertStringEqualsFile($this->file, $string . PHP_EOL);
	}

	public function testErrorLevel()
	{
		$record = new Record(new Logger(), Logger::ERROR, 'Message');
		$string = $this->handler->getFormatter()->format($record);

		$this->handler->handle($record);
		$this->assertStringEqualsFile($this->file, $string . PHP_EOL);

		$this->handler->setLevel(Logger::ERROR + 1);

		$this->handler->handle($record);
		$this->assertStringEqualsFile($this->file, $string . PHP_EOL);
	}

	public function testCriticalLevel()
	{
		$record = new Record(new Logger(), Logger::CRITICAL, 'Message');
		$string = $this->handler->getFormatter()->format($record);

		$this->handler->handle($record);
		$this->assertStringEqualsFile($this->file, $string . PHP_EOL);

		$this->handler->setLevel(Logger::CRITICAL + 1);

		$this->handler->handle($record);
		$this->assertStringEqualsFile($this->file, $string . PHP_EOL);
	}
};
